 <?php 

$array = [5, 3, 2, 8, 1, 4];

function sortArray($arr) {
    for($i=0; $i<count($arr); $i++) {
        if (isOdd($arr[$i])) {
			$oddsIndexes[] = $i;
			$oddsValues[] = $arr[$i];
        }
    }

    sort($oddsValues);

    foreach ($oddsIndexes as $i => $index) {
    	$arr[$index] = $oddsValues[$i];	
    }

    return $arr;
} 

function isOdd($number) {
	return ($number % 2);
}

var_dump(sortArray($array));