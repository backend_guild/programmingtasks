<?php

require_once __DIR__ . "/IslandsPrinterInterface.php"; 

class IslandsConsolePrinter implements IslandsPrinterInterface
{
	const SPRITES = [
		'partOfIsland' => 'X',
		'water' => '~',
	];

	function __construct($ocean) {
		$this->ocean = $ocean;
	}

	public function printEarth() {
		foreach ($this->ocean as $row) {
			foreach ($row as $oceanItem) {
				if ($this->isPartOfIsland($oceanItem)) {
					$elementName = 'partOfIsland';
				} else {
					$elementName = 'water';
				}
				$this->printItem($elementName);
			}
			echo "\n";
		}
	}

	private function printItem($item) {
		echo " " . self::SPRITES[$item] . " ";
	}

	private function isPartOfIsland($item) {
		return $item == 1;
	}
}
