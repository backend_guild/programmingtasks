<?php

require_once __DIR__ . "/IslandsConsolePrinter.php";

class IslandsCounter 
{
	private $ocean = [];
	private $islandsCount = 0;

	function __construct($ocean) {
		$this->ocean = $ocean;
		$this->islandsConsolePirnter = new IslandsConsolePrinter($ocean);
	}

	public function run() {
		$this->islandsConsolePirnter->printEarth();
		$this->count();
		$this->printIslandsCount();
	}

	private function count() {
		for ($i=0; $i<count($this->ocean); $i++) {
			for($j=0; $j<count($this->ocean[$i]); $j++) {
				if ($this->isPartOfIsland($this->ocean[$i][$j])) {
					if ($this->isNewIsland($i, $j)) {
						$this->islandsCount += 1;
					}
				}	
			}
		}
	}

	private function printIslandsCount() {
		echo "count of islands: {$this->islandsCount} \n";
	}

	//если отсутствует правый или нижний элемент, значит, остров завершен и его можно посчитать
	private function isNewIsland($i, $j) {
		return  !$this->isPartOfIsland($this->nextItem($i, $j))
				&&
				!$this->isPartOfIsland($this->nextLevelItem($i, $j));
	}

	private function nextItem($i, $j) {
		if (isset($this->ocean[$i][$j+1])) {
			return $this->ocean[$i][$j + 1];
		}
		return 0;
	}

	private function nextLevelItem($i, $j) {
		if (isset($this->ocean[$i + 1])) {
			if (isset($this->ocean[$i + 1][$j])) {
				return $this->ocean[$i + 1][$j];		
			}
		}
		return 0;
	}

	private function isPartOfIsland($item) {
		return $item == 1;
	}
}

$ocean = [
  [1, 1, 0, 0, 0, 1],
  [1, 1, 0, 0, 1, 1],
  [0, 0, 1, 0, 0, 0],
];


$islandsCounter = new IslandsCounter($ocean);
$islandsCounter->run();